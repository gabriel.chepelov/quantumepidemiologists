import random as rd
import numpy as np
from read_csv import *
import igraph as ig

graph = temporal_edges_to_array()


def degree(G, n):
    # returns the list of n nodes of highest degree
    d_degree = {}

    # the degree of a node is the number of links it has with the other nodes, so if node_i already encountered node_j, their connection
    # is saved in the following dictionnary
    already_encountered = {}

    for i in G[1:]:  # G begins to 1 because the first line of temporal_edges.csv has no values
        # if the node_i and node_j never encountered, both of their degree increase by one
        if (i[0], i[1]) not in already_encountered or (i[1], i[0]) not in already_encountered:
            if i[0] in d_degree:
                d_degree[i[0]] += 1
            else:
                d_degree[i[0]] = 1
            if i[1] in d_degree:
                d_degree[i[1]] += 1
            else:
                d_degree[i[1]] = 1

            # their meeting is saved
            already_encountered[(i[0], i[1])] = 1
            already_encountered[(i[1], i[0])] = 1

    # the following list contains the pair (node, degree) sorted by increasing degree
    d_degree_sorted = sorted(d_degree.items(), key=lambda x: x[1])

    # the list of highest degree nodes
    d_highest_degree = []

    for i in range(1, n+1):
        d_highest_degree.append(d_degree_sorted[-i][0])

    return d_highest_degree


def random(n):
    # returns a list of n students randomly choosen
    random_students = {}

    while len(random_students) < n:
        # the minus one retires the first line of the methadata.csv
        r = rd.randint(0, len(csv_to_array("data/methadata.csv"))-1)

        if r not in random_students:
            random_students[r] = True

    return random_students
