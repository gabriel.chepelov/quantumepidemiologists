from read_csv import temporal_edges_to_array, temporal_edges_to_dict
import numpy as np

def graph(): #returns the graph of connexions between people in the school
    temporal_edges_arr = temporal_edges_to_array()
    temporal_edges_dict = temporal_edges_to_dict()

    n_people = 0 #number of people in the school = 180
    for i in range(len(temporal_edges_arr)):
        if temporal_edges_arr[i][1] > n_people:
            n_people = temporal_edges_arr[i][1]
        if temporal_edges_arr[i][2] > n_people:
            n_people = temporal_edges_arr[i][2]
    n_people += 1

    G = [ {} for _ in range(n_people) ] #graph of connexions between people
    for t in temporal_edges_dict:
        for k in range(len(temporal_edges_dict[t])):
            j = temporal_edges_dict[t][k][1]
            i = temporal_edges_dict[t][k][0]
            if j < i:
                a = i
                i = j
                j = a
            assert i < j

            if not j in G[i]:
                G[i][j] = 1
                G[j][i] = 1
            else:
                G[i][j] += 1
                G[j][i] += 1
    
    for i in range(len(G)):
        for j in G[i]:
            G[i][j] = 1/G[i][j]
    
    # G[i] is a dictionary in which the keys are the neighbors of i, and the associated values are the inverse of the number of contacts between i and j, used as the weight of the relationship
    
    return G


def adjacency_matrix(): #returns the adjacency matrix of graph with 0 and 1
    G = graph()
    n = len(G)
    A = np.array( [ [0 for _ in range(n)] for _ in range(n) ] )
    for i in range(n):
        for j in range(n):
            if j in G[i]:
                A[i][j] = 1
    
    return A
