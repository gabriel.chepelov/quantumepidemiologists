import math
import copy
import graph

# A graph is defined by a dictionnary where the keys are the vertexs and the values are lists which
# contained all the neighbors of the key in the graph. The dictionnary weight refers to the weight of each
# vertexs : the keys are the edges (vertex1,vertex2) and the values the weight of the corresponding edge


# initialization of G, example of graph. You can notice that in your study, the graph is not oriented

# extract_max function used in dijkstra algorithm
from graph import *

Gr = [{1: 1/3, 3: 1/1}, {0: 1/3, 2: 1/3}, {3: 1/1, 1: 1/3}, {0: 1/1, 2: 1 /
                                                             1, 4: 1/2}, {3: 1/2, 5: 1/3, 6: 1/1}, {4: 1/3, 6: 1/2}, {4: 1/1, 5: 1/2}]

# auxiliary function used in dijkstra


def extract_min(to_visit, distance):
    dist_min = 0
    vertex = to_visit[0]
    for node in to_visit:
        if distance[node] < dist_min:
            dist_min = distance[node]
            vertex = node
    to_visit.remove(vertex)
    return vertex

# the famous dijkstra algorithm


def dijkstra(G, start):
    # initialization
    to_visit = [start]
    # 'parent' helps us to know the shortest_paths that leads to 'start'
    # parent is a dictionnary in which the values are a list of vertices that represented the precedent vertices in the different shortest paths from the start to that key.
    # As there are several potential shortest paths, a vertice can have several 'precedent' vertices (as much as the number of shortest paths)
    parent = {start: [start]}
    distance = {}
    distance[start] = 0
    # real start of the algorithm
    while len(to_visit) != 0:
        s = extract_min(to_visit, distance)
        for neighbor in G[s]:
            if neighbor not in parent:
                to_visit.append(neighbor)
            weight = G[s][neighbor]
            dist = distance[s]+weight
            d = math.inf if neighbor not in distance else distance[neighbor]
            # Due to our using of float numbers, which can lead to randomness, we consider that the length of two paths is equal if the absolute value of the difference is lower than 10**5
            if abs(dist-d) < (10**(-5)):
                (parent[neighbor]).append(s)
            elif (dist < d):
                parent[neighbor] = [s]
                distance[neighbor] = dist
    return (parent, distance)


# list where list[i] is the list of shortest paths from start to i.
def list_list_shortest_path(G, start):
    (parent, distance) = dijkstra(G, start)
    lll = []
    for vertex in range(len(G)):
        ll = list_shortest_path(G, start, vertex, parent)
        lll.append(ll)
    return lll

# list of the shortest paths from start to finish. Parent is the dictionnary returned by dijkstra


def list_shortest_path(G, start, finish, parent):
    if finish not in parent:
        return []
    elif start == finish:
        return [[start]]
    else:
        list_paths = []
        for vertex in parent[finish]:
            ll = list_shortest_path(G, start, vertex, parent)
            for i in range(len(ll)):
                ll[i].append(finish)
                list_paths += ll
        return list_paths


def length_shortest_path(G, start, finish):
    distance = dijkstra(G, start)[1]
    return distance[finish]


def nb_shortest_path_crossing_vertex(G, vertex, list_shortest_paths):
    nb_shortest_path_crossing_vertex = 0
    for i in range(len(list_shortest_paths)):
        p = list_shortest_paths[i]
        if vertex in p:
            nb_shortest_path_crossing_vertex += 1
    return (nb_shortest_path_crossing_vertex, len(list_shortest_paths))


def betweenness_centrality(G, vertex, all_shortest_paths):
    done = {}
    betweenness_centrality = 0
    n = len(G)
    for i in range(n):
        for j in range(n):
            if i != vertex and j != vertex and i != j and (i, j) not in done and (j, i) not in done:
                (x, y) = nb_shortest_path_crossing_vertex(
                    G, vertex, all_shortest_paths[i][j])
                betweenness_centrality += x/y
                done[(i, j)] = True
    return betweenness_centrality


def closeness_centrality(G, vertex):
    closeness_centrality = 0
    n = len(G)
    for i in range(n):
        if i != vertex:
            x = length_shortest_path(G, vertex, i)
            closeness_centrality += x
    return 1/closeness_centrality
