from degree_and_random import *
from read_csv import *
import numpy as np
import random as rd
import matplotlib.pyplot as plt
import friendship_paradox as fp
from conclusion_of_centralities import *

# 0 = sane
# 1 = tested contagious infected
# 2 = recovered
# 3 = exposed
# 4 = vaccinated
# 5 = untested contagious infected


T = [i for i in range(36000)]

# T represents time


def transition_SIR(personne, t, d, l, nu, mu, q):
    # applies SIR transition algorithm to every time step: we look at the interactions at every time step, and we apply probabilities for each situation

    if t in d:  # if there were interactions at this time step
        for a, b in d[t]:
            # if people a is sick undetected (detected one are quarantined) and people b is sane
            if personne[a] == 5 and personne[b] == 0 and rd.random() < l:
                personne[b] = 3
            if personne[b] == 5 and personne[a] == 0 and rd.random() < l:  # vice-versa
                personne[a] = 3

    for c in range(len(personne)):
        if personne[c] == 3 and rd.random() < nu:
            if rd.random() < q:  # the people is tested
                personne[c] = 1
            else:
                personne[c] = 5
        if (personne[c] == 1 or personne[c] == 3 or personne[c] == 5) and rd.random() < mu:
            personne[c] = 2

    return personne


def transition_SIR_vaccine(personne, t, d, l, nu, mu, q, student_vaccine):
    # idem but looking at the vaccinated

    for x in student_vaccine:
        personne[x] = 4

    if t in d:
        for couple in d[t]:
            a, b = couple
            if personne[a] == 5 and personne[b] == 0 and rd.random() < l and b not in student_vaccine:
                personne[b] = 3
            if personne[b] == 5 and personne[a] == 0 and rd.random() < l and a not in student_vaccine:
                personne[a] = 3

    for c in range(len(personne)):
        if personne[c] == 3 and rd.random() < nu:
            if rd.random() < q:  # the people is tested
                personne[c] = 1
            else:
                personne[c] = 5
        if (personne[c] == 1 or personne[c] == 3 or personne[c] == 5) and rd.random() < mu:
            personne[c] = 2

    return personne


def epidemic(personne, T, d, l, nu, mu, q):
    S = []
    E = []
    Id = []
    Iu = []
    R = []
    V = []
    for t in T:  # at each time step
        s = 0
        e = 0
        id = 0
        iu = 0
        r = 0
        v = 0
        personne = transition_SIR(personne, t, d, l, nu, mu, q)
        for p in personne:
            if p == 0:
                s += 1
            elif p == 2:
                r += 1
            elif p == 1:
                id += 1
            elif p == 3:
                e += 1
            elif p == 4:
                v += 1
            elif p == 5:
                iu += 1
        S.append(s)
        E.append(e)
        Id.append(id)
        Iu.append(iu)
        R.append(r)
        V.append(v)
    return S, E, Id, Iu, R, V


def epidemic_vaccination(personne, T, d, l, nu, mu, q, student):
    S = []
    E = []
    Id = []
    Iu = []
    R = []
    V = []
    for t in T:
        s = 0
        e = 0
        id = 0
        iu = 0
        r = 0
        v = 0
        personne = transition_SIR_vaccine(
            personne, t, d, l, nu, mu, q, student)
        for p in personne:
            if p == 0:
                s += 1
            elif p == 2:
                r += 1
            elif p == 1:
                id += 1
            elif p == 3:
                e += 1
            elif p == 4:
                v += 1
            elif p == 5:
                iu += 1
        S.append(s)
        E.append(e)
        Id.append(id)
        Iu.append(iu)
        R.append(r)
        V.append(v)
    return S, E, Id, Iu, R, V


def graphique(T, d, l, nu, mu, q, n, choice):

    personne = [0 for _ in range(180)]  # list giving students' results

    # 5 sick people at the beginning
    for _ in range(5):
        i = rd.randint(0, 180)
        personne[i] = 5
    if choice == 1:
        S, E, Id, Iu, R, V = epidemic(personne, T, d, l, nu, mu, q)
        plt.plot(T, S, label='S')
        plt.plot(T, E, label='E')
        plt.plot(T, Id, label='I$_d$')
        plt.plot(T, Iu, label='I$_u$')
        plt.plot(T, [Id[t] + Iu[t] for t in range(len(Id))], label='I')
        plt.plot(T, R,  label='R')
        plt.plot(T, V,  label='V')
        plt.legend()
        plt.grid()
        plt.title("No Vaccination \n λ  = " + str(l) + ", ν = " + str(nu) + ", μ = " +
                  str(mu) + ",\nqmin = " + str(qmin) + ", qmax = " + str(qmax) + ", q = " + str(q))
        plt.show()

    elif choice == 2:
        random_student = random(n)
        S, E, Id, Iu, R, V = epidemic_vaccination(
            personne, T, d, l, nu, mu, q, random_student)
        plt.plot(T, S, label='S')
        plt.plot(T, E, label='E')
        plt.plot(T, Id, label='I$_d$')
        plt.plot(T, Iu, label='I$_u$')
        plt.plot(T, [Id[t] + Iu[t] for t in range(len(Id))], label='I')
        plt.plot(T, R,  label='R')
        plt.plot(T, V,  label='V')
        plt.legend()
        plt.grid()
        plt.title("Random Vaccination\n λ = " + str(l) + ", ν = " + str(nu) + ", μ = " +
                  str(mu) + ",\nqmin = " + str(qmin) + ", qmax = " + str(qmax) + ", q = " + str(q))
        plt.show()

    elif choice == 3:
        degree_student = degree(graph, n)
        S, E, Id, Iu, R, V = epidemic_vaccination(
            personne, T, d, l, nu, mu, q, degree_student)
        plt.plot(T, S, label='S')
        plt.plot(T, E, label='E')
        plt.plot(T, Id, label='I$_d$')
        plt.plot(T, Iu, label='I$_u$')
        plt.plot(T, [Id[t] + Iu[t] for t in range(len(Id))], label='I')
        plt.plot(T, R,  label='R')
        plt.plot(T, V,  label='V')
        plt.legend()
        plt.grid()
        plt.title("Degree Vaccination\n λ = " + str(l) + ", ν = " + str(nu) + ", μ = " +
                  str(mu) + ",\nqmin = " + str(qmin) + ", qmax = " + str(qmax) + ", q = " + str(q))
        plt.show()

    elif choice == 4:

        L_vaccine = fp.random_walk(d, n, 10)
        S, E, Id, Iu, R, V = epidemic_vaccination(
            personne, T, d, l, nu, mu, q, L_vaccine)
        plt.plot(T, S, label='S')
        plt.plot(T, E, label='E')
        plt.plot(T, Id, label='I$_d$')
        plt.plot(T, Iu, label='I$_u$')
        plt.plot(T, [Id[t] + Iu[t] for t in range(len(Id))], label='I')
        plt.plot(T, R,  label='R')
        plt.plot(T, V,  label='V')
        plt.legend()
        plt.grid()
        plt.title("Friendship Paradox Vaccination\n λ = " + str(l) + ", ν = " + str(nu) +
                  ", μ = " + str(mu) + ",\nqmin = " + str(qmin) + ", qmax = " + str(qmax) + ", q = " + str(q))
        plt.show()

    elif choice == 5:
        student = all_closeness_centrality(graph(), n)
        S, E, Id, Iu, R, V = epidemic_vaccination(
            personne, T, d, l, nu, mu, q, student)
        plt.plot(T, S, label='S')
        plt.plot(T, E, label='E')
        plt.plot(T, Id, label='I$_d$')
        plt.plot(T, Iu, label='I$_u$')
        plt.plot(T, [Id[t] + Iu[t] for t in range(len(Id))], label='I')
        plt.plot(T, R,  label='R')
        plt.plot(T, V,  label='V')
        plt.legend()
        plt.grid()
        plt.title("Closeness Centrality Vaccination\n λ = " + str(l) + ", ν = " + str(nu) +
                  ", μ = " + str(mu) + ",\nqmin = " + str(qmin) + ", qmax = " + str(qmax) + ", q = " + str(q))
        plt.show()

    elif choice == 6:
        student = all_betwenness_centrality(graph(), n)
        S, E, Id, Iu, R, V = epidemic_vaccination(
            personne, T, d, l, nu, mu, q, student)
        plt.plot(T, S, label='S')
        plt.plot(T, E, label='E')
        plt.plot(T, Id, label='I$_d$')
        plt.plot(T, Iu, label='I$_u$')
        plt.plot(T, [Id[t] + Iu[t] for t in range(len(Id))], label='I')
        plt.plot(T, R,  label='R')
        plt.plot(T, V,  label='V')
        plt.legend()
        plt.grid()
        plt.title("Betweenness Centrality Vaccination\n λ = " + str(l) + ", ν = " + str(nu) +
                  ", μ = " + str(mu) + ",\nqmin = " + str(qmin) + ", qmax = " + str(qmax) + ", q = " + str(q))
        plt.show()


# choice 1 : without vaccination
# choice 2 : random vaccination
# choice 3 : technique degree
# choice 4 : friendship paradox
# choice 5 : closeness centrality
# choice 6 : betweenness centrality


# Graph
d = temporal_edges_to_dict()

# Parameters
l = 0.7  # lambda
nu = 0.8
mu = 0.0001
qmin = 0.2
qmax = 0.4
q = qmin + rd.random()*(qmax-qmin)  # qmin <= q <= qmax

# Representation in time
graphique(T, d, l, nu, mu, q, 20, 1)
