from centrality import *
from graph import *

Gr = [{1: 1/3, 3: 1/1}, {0: 1/3, 2: 1/3}, {3: 1/1, 1: 1/3}, {0: 1/1, 2: 1 /
                                                             1, 4: 1/2}, {3: 1/2, 5: 1/3, 6: 1/1}, {4: 1/3, 6: 1/2}, {4: 1/1, 5: 1/2}]
G = graph()  # example

# list where list[i][j] is the list of shortest paths from i to j


def all_shortest_paths(graph):
    shortest_paths = []
    for i in range(len(graph)):
        ll = list_list_shortest_path(graph, i)
        shortest_paths.append(ll)
    return shortest_paths

# list where list[i] is the betwenness centrality of i


def all_betwenness_centrality(graph, n):
    l = all_shortest_paths(graph)
    list_betwenness_centrality = []
    for i in range(len(graph)):
        list_betwenness_centrality.append(betweenness_centrality(graph, i, l))

    l = {}
    for i in range(len(list_betwenness_centrality)):
        l[i] = list_betwenness_centrality[i]
    sorted_l = sorted(l.items(), key=lambda x: x[1])
    final_list = []
    for i in range(1, n+1):
        final_list.append(sorted_l[-i][0])

    return final_list

# list where list[i] is the closeness centrality of i


def all_closeness_centrality(graph, n):
    t = []
    for i in range(len(graph)):
        t.append(closeness_centrality(graph, i))
    l = {}
    for i in range(len(t)):
        l[i] = t[i]
    sorted_l = sorted(l.items(), key=lambda x: x[1])
    final_list = []
    for i in range(1, n+1):
        final_list.append(sorted_l[-i][0])

    return final_list

# print(all_betwenness_centrality(G,10))
