import copy
import numpy.random as rd
import numpy as np

def graph_connection(time_graph):

    G_connect = {i : [0,[j for j in range(180)]] for i in range(180)}

    for time,interaction in time_graph.items():
        for p1,p2 in interaction:
            G_connect[p1][0]+=1
            G_connect[p1][1][p2]+=1
            G_connect[p2][0]+=1
            G_connect[p2][1][p1]+=1
    
    
    return G_connect

def random_walk(time_graph, nb_vaccin , n_step): #function to iterate the random propagation of vaccin 
    G_connect = graph_connection(time_graph)

    choice = rd.choice(np.array([i for i in range(180)]), nb_vaccin, replace = False) #we chose n persons randomly to get the vaccin 

    for k in range(n_step):
        prop(G_connect,choice)
    return choice

def prop(G_connect,choice): #function changing the vaccinated 

    for i in range(len(choice)): 

        person = choice[i]
        select = rd.randint(1,G_connect[person][0]+1) 
        #we want to chose one contact who will get the vaccine.
        #we select it with probability nb_contact[voisin]/nb_contact_total
        target = 0
        compteur = 0
        while compteur < select and target < 180:
            compteur += G_connect[person][1][target] 
            target +=1 
        if not target in choice and G_connect[target][0]>G_connect[choice[i]][0]: 
            #if the target has less connections, it is less interesting to give him the vaccination
            choice[i] = target 

    


