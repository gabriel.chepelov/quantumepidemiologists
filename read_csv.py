import numpy as np
import pandas as pd

temporal_edges = 'data/temporal_edges.csv'

def csv_to_array(file): #converts a .csv file into a numpy array, keeps all lines and columns
    dataset = pd.read_csv(file, header=None)
    dataframes = pd.DataFrame(dataset)
    arr = np.array(dataframes.values)
    return arr


def temporal_edges_to_array(): #returns the interesting data of file data/temporal_edges.csv in a numpy array
    raw_arr = csv_to_array(temporal_edges)
    n,p = len(raw_arr), len(raw_arr[0])
    arr = np.array( [ [ int(raw_arr[i][j]) for j in range(0,p-2) ] for i in range(1,n) ] )
    return arr


def temporal_edges_to_dict(): #returns the interesting data of file data/temporal_edges.csv in a dictionary where keys are the time of an interaction and values are the couple of the two people meeting each other
    arr = temporal_edges_to_array()
    dict = {}
    for i in range(len(arr)):
        if arr[i][0] not in dict:
            dict[arr[i][0]] = [(arr[i][1], arr[i][2])]
        else:
            dict[arr[i][0]].append((arr[i][1], arr[i][2]))
    return dict
